var questions = [
    {
        question: "How can you select an element by class 'demo' in javascript ?",
        options: [
            { txt: 'document.getElementByClass("demo")', value: 'a' },
            { txt: 'document.getElementsByClassName("demo")', value: 'b' },
            { txt: 'document.getElementByClassname("demo")', value: 'c' },
            { txt: 'document.getElementByClassName("demo")', value: 'd' }
        ],
        answer: 'b',
    },
    {
        question: "How can you select an element by id 'demo' in javascript ?",
        options: [
            { txt: 'document.getElementById("demo")', value: 'a' },
            { txt: 'document.getElementsByIdName("demo")', value: 'b' },
            { txt: 'document.getElementByIdname("demo")', value: 'c' },
            { txt: 'document.getElementByIdName("demo")', value: 'd' }
        ],
        answer: 'a',
    },
    {
        question: "How can you select all p elements in javascript ?",
        options: [
            { txt: 'document.getElementByTag("p")', value: 'a' },
            { txt: 'document.getElementByTagname("p")', value: 'b' },
            { txt: 'document.getElementsByTagName("p")', value: 'c' },
            { txt: 'document.getElementByTagName("p")', value: 'd' }
        ],
        answer: 'c',
    }
];
var timer = 10;
var count = 0;
var score = 0;
var scorePerQuestion = 10;
var color = 'red';
var questionBox = document.getElementById('question-box');
var optionBox = document.getElementById('option-box');
var progressBox = document.getElementById('progress-box');
var scoreElem = document.getElementById('score');
var timerElem = document.getElementById('timer');
var currentQuestion;

window.onload = function () {
    document.getElementById('question-number').innerHTML = questions.length;
    document.getElementById('time-per-question').innerHTML = timer;
};

document.getElementById('start').onclick = () => startQuiz();

function startQuiz() {
    document.getElementById('start').style.display = 'none';
    document.getElementById('quiz-box').style.display = 'block';
    document.getElementById("score").classList.remove("d-none");
    document.getElementById("timer").classList.remove("d-none");
    scoreElem.innerHTML = 0;
    timerElem.innerHTML = 10;
    currentQuestion = questions[count];
    count++;
    questionBox.innerHTML = currentQuestion.question;
    currentQuestion.options.forEach((element) => {
        optionBox.innerHTML += `<div class="option btn btn-primary btn-block" onclick="checkAnswer('${element.value}')">${element.txt}</div>&nbsp`;
    });
    progressBox.innerHTML = '';
    questions.forEach((question, i) => {
        progressBox.innerHTML += `<span class="svg svg-sm mx-1" id='counter-${i}'></span>`;
    });
    startTimer();
}

function checkAnswer(value) {
    color = 'red';
    if (value == currentQuestion.answer) {
        score += scorePerQuestion;
        color = 'green';
    }
    nextQuestion();
}

function nextQuestion() {
    if (count < questions.length) {
        timer = 10;
        scoreElem.innerHTML = score;
        currentQuestion = questions[count];
        document.getElementById(`counter-${count-1}`).style.backgroundColor = color;
        console.log(count-1);
        count++;
        questionBox.innerHTML = currentQuestion.question;
        optionBox.innerHTML = '';
        currentQuestion.options.forEach((element) => {
            optionBox.innerHTML += `<div class="option btn btn-primary btn-block" onclick="checkAnswer('${element.value}')">${element.txt}</div>&nbsp`;
        });
    }else{
        endQuiz();
    }
}

function startTimer() {
    if (count < questions.length) {
        setInterval(function () {
            timer--;
            timerElem.innerHTML = timer;
            if (timer <= 0) {
                timerElem.innerHTML = 10;
                nextQuestion();
            }
        }, 1000)
    }
}

function endQuiz() {
    document.getElementById('quiz-area').style.display = "none";
    document.getElementById('result-box').classList.remove('d-none');
    document.getElementById('message').innerHTML = (score / 10) > Math.round(questions.length / 2) ? "Congratulations" : "Oops";
    document.getElementById('brek-down').innerHTML = `Your score : ${score}`;
}